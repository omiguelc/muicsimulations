//
// Created by omiguelc on 08/02/22.
//

#ifndef MUICPYTHIASIMULATIONS_SIMULATION_H
#define MUICPYTHIASIMULATIONS_SIMULATION_H

#include <string>

class Simulation {

public:
    Simulation();

    ~Simulation() = default;

    const std::string &getOutputPath() const;

    void setOutputPath(const std::string &);

    int getNEvents() const;

    void setNEvents(int);

    std::string getLHEInputFile() const;

    void setLHEInputFile(std::string);

    double getEProton() const;

    void setEProton(double);

    double getEMuon() const;

    void setEMuon(double);

    double getQ2Min() const;

    void setQ2Min(double);

    double getYMin() const;

    void setYMin(double);

    double getYMax() const;

    void setYMax(double);

    bool isUseColorReconnectionFixEn() const;

    void setUseColorReconnectionFixEn(bool);

    bool isUseAntimuonEn() const;

    void setUseAntimuonEn(bool);

    bool isUseChargedCurrentEn() const;

    void setUseChargedCurrentEn(bool);

    bool isUseNeutralCurrentEn() const;

    void setUseNeutralCurrentEn(bool);

    void run();

private:
    std::string output_path_root_;
    std::string output_path_hepmc_;

    int n_events_;
    std::string lhe_input_fpath_;
    double e_proton_; // GeV
    double e_muon_;   // GeV
    double Q2_min_;   // GeV^2
    double y_min_;
    double y_max_;
    bool use_color_reconnection_fix_en_;
    bool use_antimuon_en_;
    bool use_charged_current_en_;
    bool use_neutral_current_en_;
};

#endif // MUICPYTHIASIMULATIONS_SIMULATION_H
